#[cfg(test)]
mod tests {
    use matrix_modbot::utils::utils::detect_caps;

    #[test]
    fn test_caps_detection() {
        assert!(detect_caps("FULL CAPSSSSSS"));
        assert!(!detect_caps("Not Full Caps But There Are Some Caps"));
        assert!(detect_caps("CAPs BUT N0T FuLLY"));
    }
}
