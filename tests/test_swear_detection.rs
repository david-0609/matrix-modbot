#[cfg(test)]
mod tests {
    use matrix_modbot::config_reader::config_reader::BotUserInfo;
    use matrix_modbot::utils::utils::{create_swear_list, detect_swear_from_message};

    #[tokio::test]
    async fn test_detect_swear() {
        let creds = BotUserInfo::get_info("tests/test_creds.ini").unwrap();
        let swear_list = create_swear_list(&creds.swear_list_url).await.unwrap();
        assert!(detect_swear_from_message(&swear_list, "fuck you"));
        assert!(detect_swear_from_message(&swear_list, "FUCK YOU IN CAPS"));
        assert!(!detect_swear_from_message(
            &swear_list,
            "This isn't a swear"
        ));
    }
}
