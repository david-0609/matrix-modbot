#[cfg(test)]
mod tests {
    use matrix_modbot::config_reader::config_reader;

    #[test]
    fn test_read_config() {
        let expected_user_id = "john";
        let expected_password = "correcthorsebatterystaple";
        let expected_homeserver = "http://example.com";
        let expected_room = "general";
        let expected_swear_list_url =
            "https://raw.githubusercontent.com/chucknorris-io/swear-words/master/en";
        let expected_allow_swear = false;
        let config_file = "tests/test_creds.ini";
        let creds = config_reader::BotUserInfo::get_info(config_file).unwrap();
        assert_eq!(expected_user_id, creds.user_id);
        assert_eq!(expected_password, creds.password);
        assert_eq!(expected_homeserver, creds.homeserver);
        assert_eq!(expected_room, creds.room);
        assert_eq!(expected_swear_list_url, creds.swear_list_url);
        assert_eq!(expected_allow_swear, creds.allow_swear)
    }
}
