# Matrix Modbot

Matrix Modbot is a bot for Matrix used to automatically moderate Matrix rooms. 

## Who is this bot for?

The bot is orientated towards admins of large Matrix rooms that are public and require constant moderation. With this bot, simple jobs such as swear filtering and spam prevention could be accomplished without intervention of human moderators. This project is still a work in progress, please feel free to PR and create issues if you find any.

## How to run?

**Using Docker:**
A minimal Docker image containing the compiled binary is published on [Docker Hub](https://hub.docker.com/r/davidlocalhost/matrix-modbot). Note that this image is automatically pushed using a CI/CD Pipeline for every commit made to the main branch. Expect there to be bugs and breaking changes. If you want to use a stable release, go to Deployments -> Releases and download the source code and build a Docker image containing a release binary.
`sudo docker run -v /local/path/to/config.ini:/config.ini -t matrix-modbot:latest`

Without root:
`docker run -v /local/path/to/config.ini:/config.ini -t matrix-modbot:latest`

**Using Cargo:**
```
cargo build --release
cargo run /path/to/config.ini
``` 

Note that this bot will NOT run in an encrypted room. It also can only join 1 room at a time and isn't integrated with Element's Spaces feature. You must specify the room to join in the config file and invite the bot. You will also have to create an account for the bot and put the credentials into the config file. It is possible to use this bot with Matrix-Discord bridge, as long as the bridge bot is set to have moderator privileges.

### Configuration
See example below
```
[credentials]
user_id = @bot:matrix.org
homeserver = matrix.org
password = insert-password-here

[room]
room = !insert-room-id:matrix.org

[swear_list]
url = https://raw.githubusercontent.com/chucknorris-io/swear-words/master/en

[rules]
allow_swear = false
```
Note that the configuration file should be specified if the file name is not `config.ini`.

## Features

- Configurable swear detection based on list of blocked words
- Spam detection
- Anti-Caps
- Anti-ASCII Art Spam
- Reputation system
  - All members of a room have reputation points, they are deducted when spam/swear/caps are detected
  - Automatically kicks a member if reputation is below -15
  - Members can award each other with maximum 1 reputation point every 24hr
    - Users with power level >= 50 are not affected by the limit (Mods and Admins)
    - Mods can warn specific members by deducting reputation points

### Commands
For awarding someone reputation: 
- "!modbot award @user:matrix.org"

For deducting someone's reputation (moderators only):
- "!modbot warn @user:matrix.org"

To get own reputation:
- "!modbot reputation"

### Credits
Many thanks to:
- [brokenbyte](https://gitlab.com/brokenbyte) for all the help and support along the way
- [hebbot](https://github.com/haecker-felix/hebbot), as an example I could follow during the development of this bot
