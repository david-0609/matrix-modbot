# # syntax=docker/dockerfile:1
FROM debian:stable-slim
ARG package
ADD $package ./
RUN chmod +x matrix-modbot
RUN touch config.ini
CMD ["./matrix-modbot", "config.ini"]
