#[allow(clippy::module_inception)]
pub mod config_reader {
    use configparser::ini::Ini;
    use std::error::Error;

    #[derive(Debug, Clone)]
    #[allow(dead_code)]
    pub struct BotUserInfo {
        pub user_id: String,
        pub password: String,
        pub homeserver: String,
        pub room: String,
        pub swear_list_url: String,
        pub allow_swear: bool,
    }

    impl BotUserInfo {
        pub fn get_info(config_file_path: &str) -> Result<Self, Box<dyn Error>> {
            let mut config = Ini::new();
            let _map = config.load(config_file_path)?;
            let user_id = config.get("credentials", "user_id").unwrap();
            let password = config.get("credentials", "password").unwrap();
            let homeserver = config.get("credentials", "homeserver").unwrap();
            let room = config.get("room", "room").unwrap();
            let swear_list_url = config.get("swear_list", "url").unwrap();
            let mut allow_swear = config.getbool("rules", "allow_swear").unwrap().unwrap();
            if allow_swear {
                allow_swear = false
            }
            println!("Read configurations");
            Ok(BotUserInfo {
                user_id,
                password,
                homeserver,
                room,
                swear_list_url,
                allow_swear,
            })
        }
    }
}
