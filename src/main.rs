use std::env;

mod bot;
mod config_reader;
mod utils;
pub use bot::bot::Bot;
pub use config_reader::config_reader::BotUserInfo;
pub use utils::utils::*;

#[tokio::main]
async fn main() {
    println!("Starting Matrix-Modbot");
    let mut args: Vec<String> = env::args().collect();
    if args.len() > 2 {
        panic!("Please only pass in 1 argument!");
    }
    if args.is_empty() {
        args.push("config.ini".to_string()) // Set the default config file path to config.ini
    }
    let config_path = &args[1];
    let creds = BotUserInfo::get_info(config_path).unwrap();
    let _ = Bot::bot_login(creds).await;
}
