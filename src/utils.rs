// Snippet from https://github.com/haecker-felix/hebbot, licensed under AGPL, License terms apply
#[allow(clippy::module_inception)]
pub mod utils {
    use matrix_sdk::reqwest;
    use matrix_sdk::ruma::events::room::message::{
        MessageType, OriginalSyncRoomMessageEvent, Relation, TextMessageEventContent,
    };
    use matrix_sdk::ruma::OwnedEventId;
    use sled::IVec;
    use std::{
        fs::File,
        io::{copy, BufRead, BufReader, Result},
    };

    pub fn get_message_event_text(event: &OriginalSyncRoomMessageEvent) -> Option<String> {
        if let MessageType::Text(TextMessageEventContent { body, .. }) = &event.content.msgtype {
            Some(body.to_owned())
        } else {
            None
        }
    }

    // A simplified way of getting an edited message
    pub fn get_edited_message_event_text(
        event: &OriginalSyncRoomMessageEvent,
    ) -> Option<(OwnedEventId, String)> {
        if let Some(Relation::Replacement(r)) = &event.content.relates_to {
            if "m.text" == r.new_content.msgtype() {
                return Some((r.event_id.clone(), r.new_content.body().to_owned()));
            }
        }

        None
    }

    pub async fn create_swear_list(swear_list_url: &str) -> Result<Vec<String>> {
        // Downloads the text file
        println!("Creating swear list");
        let resp = reqwest::get(swear_list_url).await.expect("request failed");
        let body = resp.text().await.expect("Invalid Body");
        let mut out = File::create("list.txt").expect("failed to create file");
        copy(&mut body.as_bytes(), &mut out).expect("failed to copy content");

        // Reads file, returns vector of Strings through collecting iterator
        BufReader::new(File::open("list.txt")?).lines().collect()
    }

    pub fn detect_swear_from_message(swear_list: &Vec<String>, message: &str) -> bool {
        let lowercase_message = message.to_lowercase();
        let message_words = lowercase_message.split_whitespace();
        for words in message_words {
            for swear in swear_list {
                if words.contains(swear) {
                    return true;
                }
            }
        }
        false
    }

    pub fn convert_from_bytes_sled(bytes: &IVec) -> (i64, i64) {
        let timestamp: &[u8; 8] = bytes[..8].try_into().unwrap();
        println!("{:?}", timestamp);
        let reputation: &[u8; 8] = bytes[8..].try_into().unwrap();
        let time_integer: i64 = i64::from_be_bytes(*timestamp);
        let rep_integer: i64 = i64::from_be_bytes(*reputation);
        (time_integer, rep_integer)
    }

    pub fn convert_to_bytes_sled(timestamp: i64, reputation: i64) -> [u8; 16] {
        let timestamp: [u8; 8] = timestamp.to_be_bytes();
        let reputation: [u8; 8] = reputation.to_be_bytes();
        let bytes: &[u8; 16] = &[timestamp, reputation].concat().try_into().unwrap();
        *bytes
    }

    pub fn convert_vec_to_str(input: &str) -> Vec<i64> {
        // Implementation 1:
        // input
        //     .split_at(input.len() - 1)
        //     .0
        //     .split_at(1)
        //     .1 // Deletes the first and last character of a string (deletes the brackets)
        //     .chars()
        //     .filter(|c| !c.is_whitespace()) // Deletes all whitespaces
        //     .collect::<String>()
        //     .as_str()
        //     .split(',')
        //     .collect::<Vec<&str>>()
        //     .iter_mut()
        //     .map(|x| x.parse::<i64>().unwrap()) // Changes from &str to i64
        //     .collect::<Vec<i64>>();
        //
        // Improved implementation
        if !input.is_empty() && input != "[]" {
            input[1..input.len() - 1]
                .split(',')
                .map(|n| n.trim().parse().unwrap())
                .collect()
        } else {
            vec![]
        }
    }

    pub fn detect_caps(message: &str) -> bool {
        let characters = message.chars().collect::<Vec<char>>();
        let msg_length: f32 = characters.len() as f32;
        let mut counter: f32 = 0.0;
        for char in characters {
            if !char.is_lowercase() && char.is_alphabetic() {
                counter += 1.0
            }
        }

        (counter / msg_length) >= 0.65
    }
}
