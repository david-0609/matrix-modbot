#![allow(clippy::collapsible_if)]
#[allow(clippy::module_inception, dead_code)]
pub mod bot {

    use std::ops::Deref;
    use std::str;

    use log::{debug, info};
    use matrix_sdk::{
        config::{RequestConfig, SyncSettings},
        event_handler::Ctx,
        room::{Joined, Room, RoomMember},
        ruma::{
            events::room::message::{OriginalSyncRoomMessageEvent, RoomMessageEventContent},
            OwnedEventId, RoomId, ServerName, UserId,
        },
        Client,
    };

    use crate::config_reader::config_reader::BotUserInfo;
    use crate::utils::utils::*;
    use chrono::{DateTime, Utc};
    use sled;

    #[allow(dead_code)]
    #[derive(Clone)]
    pub struct Bot {
        client: Client,
        info: BotUserInfo,
        joined_room: Joined,
        swear_list: Vec<String>,
        database_handle: sled::Db,
        spam_db_handle: sled::Db,
        members_list: Vec<String>,
    }

    impl Bot {
        pub async fn bot_login(creds: BotUserInfo) -> Self {
            let user_id = creds.user_id.as_str();
            let password = creds.password.as_str();
            let room = RoomId::parse(creds.room.as_str()).unwrap();
            let user = UserId::parse(user_id).expect("Unable to parse bot user id");
            let server_name = ServerName::parse(user.server_name()).unwrap();
            let request_config = RequestConfig::new().force_auth();
            let client = Client::builder()
                .server_name(&server_name)
                .request_config(request_config)
                .build()
                .await
                .unwrap();

            info!("Logging in…");
            let response = client
                .login_username(user.localpart(), password)
                .initial_device_display_name("Matrix-Modbot")
                .send()
                .await
                .expect("Unable to login");

            info!("Doing the initial sync…");
            client
                .sync_once(SyncSettings::new())
                .await
                .expect("Unable to sync");

            info!(
                "Logged in as {}, got device_id {}",
                response.user_id, response.device_id
            );

            // Code snippet from hebbot https://github.com/haecker-felix/hebbot
            // Try to accept room invite, if any
            if let Some(invited_room) = client.get_invited_room(&room) {
                invited_room
                    .accept_invitation()
                    .await
                    .expect("Matrix-Modbot could not join the invited room");
            }

            // Need to do another sync to make sure bot knows the newly joined rooms
            client
                .sync_once(SyncSettings::new())
                .await
                .expect("Unable to sync");

            let joined_room = client
                .get_joined_room(&room)
                .expect("Cannot get joined room");

            let swear_list = create_swear_list(creds.swear_list_url.as_str())
                .await
                .expect("Could not download swear list");

            // Syncs members list of the joined room
            joined_room.sync_members().await.unwrap();

            let members_list: Vec<String> = client
                .get_joined_room(joined_room.room_id())
                .unwrap()
                .joined_members()
                .await
                .unwrap()
                .iter()
                .map(|x| x.user_id().as_str().to_string())
                .collect();

            let path = "members_database";
            let db = sled::open(path).unwrap();
            let default_reputation: i64 = 0;
            let utc: DateTime<Utc> = Utc::now();
            let timestamp: i64 = utc.timestamp();
            let bytes = convert_to_bytes_sled(timestamp, default_reputation);
            let spam_path = "spam_tracking_database";
            let spam_db_handle = sled::open(spam_path).unwrap();
            for member in members_list.clone() {
                if member != creds.user_id {
                    {
                        dbg!(db.insert(member.as_str(), &bytes).unwrap());
                    }
                    {
                        dbg!(spam_db_handle
                            .insert(member.as_str(), "[]".as_bytes())
                            .unwrap());
                    }
                }
            }
            let bot = Bot {
                client,
                info: creds,
                joined_room,
                swear_list: swear_list.clone(),
                database_handle: db,
                spam_db_handle,
                members_list: members_list.clone(),
            };

            bot.send_message("Sucessfully started Matrix-Modbot").await;

            bot.client.add_event_handler_context(bot.clone());
            bot.client.add_event_handler(Self::on_room_message);

            info!("Started syncing");
            bot.client.sync(SyncSettings::new()).await.unwrap();

            bot
        }

        async fn on_room_message(
            event: OriginalSyncRoomMessageEvent,
            room: Room,
            Ctx(mut bot): Ctx<Bot>,
        ) {
            if let Room::Joined(_joined) = &room {
                // Standard text message
                if let Some(text) = get_message_event_text(&event) {
                    let member = room.get_member(&event.sender).await.unwrap().unwrap();
                    let member_user_id = member.user_id().as_str().to_string();
                    let id = &event.event_id;

                    if !bot.members_list.contains(&member_user_id) {
                        bot.members_list.push(member_user_id.clone());
                        let default_reputation: i64 = 0;
                        let utc: DateTime<Utc> = Utc::now();
                        let timestamp: i64 = utc.timestamp();
                        let bytes = convert_to_bytes_sled(timestamp, default_reputation);
                        dbg!(bot
                            .database_handle
                            .insert(member_user_id.as_str(), &bytes)
                            .unwrap());
                    };

                    if !bot.info.allow_swear && detect_swear_from_message(&bot.swear_list, &text) {
                        bot.delete_message_from_room(id, "Swearing").await;
                        bot.update_reputation_for_member(&member, -1)
                            .await
                            .expect("Failed to subtract reputation from member");
                    };
                    bot.detect_spam(&event).await;
                    bot.detect_command(&event, &text).await;
                    if bot.detect_whitespace_spam(&text) {
                        bot.delete_message_from_room(id, "Spamming").await;
                        bot.update_reputation_for_member(&member, -1)
                            .await
                            .expect("Failed to subtract reputation from member");
                    }

                    if detect_caps(&text) {
                        bot.delete_message_from_room(id, "Caps").await;
                        bot.update_reputation_for_member(&member, -1)
                            .await
                            .expect("Failed to subtract reputation from member");
                    }
                }
            }

            // Message edit
            if let Some((edited_msg_event_id, text)) = get_edited_message_event_text(&event) {
                let member = room.get_member(&event.sender).await.unwrap().unwrap();
                if bot.info.allow_swear && detect_swear_from_message(&bot.swear_list, &text) {
                    bot.delete_message_from_room(&edited_msg_event_id, "Swearing")
                        .await;
                    bot.update_reputation_for_member(&member, -1)
                        .await
                        .expect("Failed to subtract reputation from member");
                };
                if detect_caps(&text) {
                    bot.delete_message_from_room(&edited_msg_event_id, "Caps")
                        .await;
                    bot.update_reputation_for_member(&member, -1)
                        .await
                        .expect("Failed to subtract reputation from member");
                }
            }
        }

        fn detect_whitespace_spam(&self, message: &str) -> bool {
            let mut counter: f32 = 0.0;
            if message.len() >= 20 {
                for char in message.chars() {
                    if char.is_ascii_whitespace() || char.is_ascii_punctuation() {
                        counter += 1.0
                    }
                }
            }
            counter / message.len() as f32 >= 0.8
        }

        async fn delete_message_from_room(&self, event_id: &OwnedEventId, reason: &str) {
            let result = self.joined_room.redact(event_id, Some(reason), None).await;
            if result.is_err() {
                self.send_message("Error deleting message").await;
            };
            self.send_message(&format!("{} is not permitted on this server", reason))
                .await;
        }

        fn check_if_member_exists(&self, user_id: &str) -> bool {
            self.members_list.contains(&user_id.to_string())
        }

        async fn update_reputation_for_member(
            &self,
            member: &RoomMember,
            rep_change: i64,
        ) -> sled::Result<()> {
            let member_id: &str = member.user_id().as_str();
            if let Some(user_data) = dbg!(self.database_handle.get(member_id)?) {
                let (timestamp, mut reputation) = convert_from_bytes_sled(&user_data);
                if reputation == -15 {
                    self.kick_user(member, "Reputation too low!").await;
                    return Ok(());
                } else {
                    reputation += rep_change;
                    let new_data = convert_to_bytes_sled(timestamp, reputation);
                    dbg!(self.database_handle.remove(member_id)?);
                    dbg!(self.database_handle.insert(member_id, &new_data)?);
                    self.send_message(&format!(
                        "{}'s current reputation is {}",
                        member_id, reputation
                    ))
                    .await;
                    return Ok(());
                }
            }
            Ok(())
        }

        async fn kick_user(&self, member: &RoomMember, reason: &str) {
            let member_id: &str = member.user_id().as_str();
            if member.power_level() <= 50 {
                // Won't kick mods and admins
                if (self
                    .joined_room
                    .kick_user(member.user_id(), Some(reason))
                    .await)
                    .is_ok()
                {
                    dbg!(self.database_handle.remove(member_id).unwrap());
                    self.send_message(&format!("Member {} has been kicked.", member_id))
                        .await;
                }
            } else {
                self.send_message("Cannot kick moderators and admins").await;
            }
        }

        async fn send_message(&self, message: &str) {
            debug!("Send message ({:?}): {}", &self.joined_room, message);
            let content = RoomMessageEventContent::text_plain(message);
            let _ = &self.joined_room.send(content, None).await.unwrap();
        }

        async fn detect_spam(&mut self, event: &OriginalSyncRoomMessageEvent) {
            if let Some(author) = self.joined_room.get_member(&event.sender).await.unwrap() {
                let author_name = author.user_id().as_str().to_string();
                let curr_utc = Utc::now().timestamp();
                let expire_time: i64 = curr_utc - 5;
                let mut expired_msgs: Vec<i64> = vec![];

                let spam_data = self.spam_db_handle.get(&author_name);
                match spam_data {
                    Ok(_) => {
                        if spam_data.clone().unwrap().is_some() {
                            let mut data_vec = convert_vec_to_str(
                                str::from_utf8(&spam_data.unwrap().unwrap()[..])
                                    .unwrap()
                                    .as_ref(),
                            );
                            if !data_vec.is_empty() {
                                for time in &data_vec {
                                    if time < &expire_time {
                                        expired_msgs.push(*time)
                                    }
                                }

                                for msg in expired_msgs {
                                    let _ = &data_vec.retain(|value| *value != msg);
                                }
                            }

                            data_vec.push(curr_utc);

                            if data_vec.len() > 3 && author_name != self.info.user_id {
                                self.delete_message_from_room(&event.event_id, "Spamming")
                                    .await;
                                self.update_reputation_for_member(&author, -1)
                                    .await
                                    .unwrap();
                            }
                            dbg!(self
                                .spam_db_handle
                                .insert(&author_name, format!("{:?}", data_vec).as_str().as_bytes())
                                .unwrap());
                        } else {
                            dbg!(self
                                .spam_db_handle
                                .insert(
                                    &author_name,
                                    format!("{:?}", vec![curr_utc]).as_str().as_bytes()
                                )
                                .unwrap());
                        }
                    }
                    Err(_) => {
                        dbg!(self
                            .spam_db_handle
                            .insert(&author_name, "[]".as_bytes())
                            .unwrap());
                    }
                }
            } else {
                self.send_message("Problem getting author of message").await;
            }
        }

        async fn detect_command(&self, event: &OriginalSyncRoomMessageEvent, message: &str) {
            let words: Vec<&str> = message.split_whitespace().collect();
            let member = self
                .joined_room
                .get_member(&event.sender)
                .await
                .unwrap()
                .unwrap();
            if words.first().unwrap().deref() == "!modbot" {
                if words[1] == "award" || words[1] == "warn" {
                    let result = dbg!(self.database_handle.get(words[2]));
                    match result {
                        Ok(Some(_)) => {
                            let (timestamp, _) = convert_from_bytes_sled(&result.unwrap().unwrap());
                            let curr_utc = Utc::now().timestamp();
                            if self.check_if_member_exists(words[2]) {
                                if curr_utc - timestamp > 86400 || member.power_level() >= 50 {
                                    // Does
                                    // not affect room admins
                                    let user_data =
                                        dbg!(self.database_handle.get(words[2]).unwrap());
                                    let (timestamp, mut reputation) =
                                        convert_from_bytes_sled(&user_data.unwrap());
                                    if words[1] == "warn" && member.power_level() >= 50 {
                                        reputation -= 1
                                    }
                                    if words[1] == "warn" && member.power_level() < 50 {
                                        self.send_message(
                                            "This command is for moderators and admins only!",
                                        )
                                        .await;
                                    }
                                    if words[1] == "award" {
                                        reputation += 1;
                                    }
                                    let new_data = convert_to_bytes_sled(timestamp, reputation);
                                    dbg!(self.database_handle.remove(words[2]).unwrap());
                                    dbg!(self.database_handle.insert(words[2], &new_data).unwrap());
                                    self.send_message(&format!(
                                        "{}'s current reputation is {}",
                                        words[2], reputation
                                    ))
                                    .await;
                                } else {
                                    let mut time_left = (curr_utc - timestamp) / 60;
                                    time_left = 24 - time_left;
                                    self.send_message(
                                        format!("A user can only get 1 reputation awarded to them every 24 hours. Please wait {} more hours.", time_left)
                                            .as_str())
                                    .await;
                                }
                            }
                        }

                        Ok(None) => {
                            self.send_message(
                                "Cannot find user. Check if the User ID provided is correct",
                            )
                            .await;
                        }

                        Err(_) => {
                            self.send_message(
                                "Cannot find user. Check if the User ID provided is correct",
                            )
                            .await;
                        }
                    };
                };
                if words[1] == "reputation" {
                    let author = self
                        .joined_room
                        .get_member(&event.sender)
                        .await
                        .unwrap()
                        .unwrap();
                    if let Some(user_data) =
                        dbg!(self.database_handle.get(author.user_id().as_str()).unwrap())
                    {
                        let (_, reputation) = convert_from_bytes_sled(&user_data);
                        self.send_message(
                            format!("Your current reputation is: {}", reputation).as_str(),
                        )
                        .await;
                    } else {
                        self.send_message("Error getting reputation").await;
                    }
                };
            }
        }
    }
}
